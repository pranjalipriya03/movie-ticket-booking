import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { ConnectionModule } from './connection/connection.module';
import { ShowsModule } from './modules/shows/shows.module';
import { ScreensModule } from './modules/screens/screens.module';
import { BookingsModule } from './modules/bookings/bookings.module';

@Module({
  imports: [ConfigModule.forRoot({ isGlobal: true }), ConnectionModule, ShowsModule, ScreensModule, BookingsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
