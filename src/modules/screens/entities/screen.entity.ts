import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Show } from '../../shows/entities/show.entity';

@Entity()
export class Screen {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  totalSeats: number;

  @OneToMany(() => Show, (show) => show.screen)
  shows: Show[];

}
