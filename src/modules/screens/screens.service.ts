import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Screen } from './entities/screen.entity';


@Injectable()
export class ScreensService {
  constructor(
    @InjectRepository(Screen)
    private readonly screensRepository: Repository<Screen>,
  ) { }


  async create(data: any) {
    return await this.screensRepository.save(data);
  }

  async findAll() {
    return await this.screensRepository.find();
  }

  async findOne(id: number) {
    return await this.screensRepository.findOne({ where: { id } });
  }

}
