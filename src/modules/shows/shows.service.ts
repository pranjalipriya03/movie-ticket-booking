import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Show } from './entities/show.entity';


@Injectable()
export class ShowsService {
  constructor(
    @InjectRepository(Show)
    private readonly showsRepository: Repository<Show>,
  ) { }

  async create(data: any) {
    return await this.showsRepository.save(data);
  }

  async findAll() {
    return await this.showsRepository.find();
  }

  async totalSeats(showId: number) {
    return await this.showsRepository.createQueryBuilder('show')
      .leftJoinAndSelect('show.screen', 'screen')
      .where('show.id = :showId', { showId })
      .getOne();
  }

  async getNextAvailability(showId: number) {
    const show = await this.showsRepository.findOne({ where: { id: showId } });

    const nextShow = await this.showsRepository
      .createQueryBuilder('show')
      .where('show.startTime > :currentStartTime', { currentStartTime: show.startTime })
      .orderBy('show.startTime', 'ASC')
      .skip(1)
      .getOne();


    return nextShow ? nextShow.startTime : null;
  }
}


