import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany } from 'typeorm';
import { Screen } from '../../screens/entities/screen.entity';
import { Booking } from '../../bookings/entities/booking.entity';

@Entity()
export class Show {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  movieName: string;

  @Column()
  screenId: number;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  startTime: Date

  @ManyToOne(() => Screen, (screen) => screen.shows)
  screen: Screen;

  @OneToMany(() => Booking, (booking) => booking.show)
  bookings: Booking[];
}
