import { Controller, Get, Post, Body } from '@nestjs/common';
import { ShowsService } from './shows.service';

@Controller('shows')
export class ShowsController {
  constructor(private readonly showsService: ShowsService) { }

  @Post()
  async create(@Body() createShowDto: any) {
    return await this.showsService.create(createShowDto);
  }

  @Get()
  async findAll() {
    return await this.showsService.findAll();
  }
}
