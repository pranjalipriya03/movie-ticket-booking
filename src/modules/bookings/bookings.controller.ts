import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { BookingsService } from './bookings.service';
import { CreateBookingDto } from './dto/create-booking.dto';
import { UpdateBookingDto } from './dto/update-booking.dto';

@Controller('bookings')
export class BookingsController {
  constructor(private readonly bookingsService: BookingsService) { }

  @Post()
  async create(@Body() data: any) {
    return await this.bookingsService.create(data.name, data.showId, data.seatNumber);
  }

  @Patch('cancel/:id')
  async remove(@Param('id') id: string) {
    return await this.bookingsService.cancel(+id);
  }
}
