import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Show } from '../../shows/entities/show.entity';


@Entity()
export class Booking {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: '' })
  name: string;

  @Column()
  showId: number;

  @Column()
  seatNumber: number;

  @Column({ default: false })
  isCancel: boolean;

  @ManyToOne(() => Show, (show) => show.bookings)
  show: Show;


}
