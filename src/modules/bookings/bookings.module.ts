import { Module } from '@nestjs/common';
import { BookingsService } from './bookings.service';
import { BookingsController } from './bookings.controller';
import { Booking } from './entities/booking.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ScreensService } from '../screens/screens.service';
import { ScreensModule } from '../screens/screens.module';
import { Screen } from '../screens/entities/screen.entity';
import { Show } from '../shows/entities/show.entity';
import { ShowsModule } from '../shows/shows.module';
import { ShowsService } from '../shows/shows.service';

@Module({
  imports: [TypeOrmModule.forFeature([Booking, Screen, Show]), ScreensModule, ShowsModule],
  controllers: [BookingsController],
  providers: [BookingsService, ScreensService, ShowsService]
})
export class BookingsModule { }
