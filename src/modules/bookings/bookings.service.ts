import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Booking } from './entities/booking.entity';
import { ScreensService } from '../screens/screens.service';
import { ShowsService } from '../shows/shows.service';


@Injectable()
export class BookingsService {
  constructor(
    @InjectRepository(Booking)
    private readonly bookingsRepository: Repository<Booking>,
    private readonly screenService: ScreensService,
    private readonly showService: ShowsService
  ) { }

  async create(name: string, showId: number, seatNumber: [number]) {
    try {
      let existingBooking;
      for (const seat of seatNumber) {
        existingBooking = await this.bookingsRepository.findOne({
          where: { showId, seatNumber: seat, isCancel: false },
        });
      }

      if (existingBooking) {
        throw new HttpException('One or more seat are already booked. Please select another seat', HttpStatus.BAD_REQUEST);
      }

      const bookedSeatsCount = await this.bookingsRepository.count({
        where: { showId, isCancel: false },
      });

      const totalSeat = await this.totalSeats(showId)
      const nextAvailability = await this.showService.getNextAvailability(showId);
      const invalidSeats = seatNumber.filter(seat => seat > totalSeat);
      if (invalidSeats.length > 0) {
        throw new HttpException("The seat you're trying to book don't exist.", HttpStatus.NOT_FOUND);
      }
      if (bookedSeatsCount >= totalSeat) {
        if (nextAvailability) {
          return `All seats for this show are booked. Next availability: ${nextAvailability}`;
        }
      }

      const bookings = seatNumber.map(seat => ({ name, showId, seatNumber: seat }));
      const booking = await this.bookingsRepository.save(bookings);
      return { booking }
    }

    catch (e) {
      throw (e)
    }
  }

  async cancel(id: number) {
    const booking = await this.bookingsRepository.findOne({ where: { id } });
    if (!booking) {
      throw new HttpException('Booking not found.', HttpStatus.NOT_FOUND);
    }
    return await this.bookingsRepository.update({ id }, { isCancel: true });;
  }

  async totalSeats(showId: number) {
    const show = await this.showService.totalSeats(showId)
    if (!show) {
      throw new HttpException('Show not found.', HttpStatus.NOT_FOUND);
    }
    return show.screen.totalSeats;
  }
}



